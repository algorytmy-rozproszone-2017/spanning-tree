Find spanning tree of a graph in a distributed way. In other words, for each node v we want:
Input: list of v neighbours
Output: list of v children and its parent (could be v if it is the root of tree)
Graph on output should be spanning tree of input graph G. You should also know (and be able to explain) what is the time and communication complexity of your solution :) 