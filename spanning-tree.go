package main

import "fmt"
import (
	"path/filepath"
	"os"
	"bufio"
	"strings"
	"strconv"
	"time"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func readGraph(resurcePath string) [][]int {
	var edges [][]int
	absPath, _ := filepath.Abs(resurcePath)
	file, err := os.Open(absPath)
	check(err)
	defer file.Close()
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		var neighbours []int
		for _, strNum := range strings.Split(scanner.Text(), " "){
			num, err := strconv.Atoi(strNum)
			check(err)
			neighbours = append(neighbours, num)
		}
		edges = append(edges, neighbours)
	}
	return edges
}

func root(id int, you_are_my_parent chan int, neighbours[] int, search_channels []chan int, parent_set_channel chan bool, done_channel chan bool){
	var children []int

	for _, elem := range neighbours{
		search_channels[elem] <- id
	}
	//fmt.Printf("Root wysłał\n")
	parent_set_channel <- true
	for {
		child, more := <- you_are_my_parent
		if more {
			children = append(children, child)
		} else {
			break
		}
	}
	fmt.Printf("%d)\tparent=%d,\tchildren=%v\n",id, id, children)
	done_channel <- true
}

func node(id int, neighbours[] int, search_channels []chan int, you_are_my_parent_channels []chan int, parent_set_channel chan bool, done_channel chan bool){
	var children []int

	parent := <-search_channels[id]
	for _, elem := range neighbours {
		select {
		case search_channels[elem] <- id:
			//fmt.Printf("%d) sending SEARCH to %d\n", id, elem)
		default:
			time.Sleep(50 * time.Millisecond)
			//fmt.Printf("%d) %d has already received SEARCH\n", id, elem)
		}
	}
	//fmt.Printf("%d wysłał\n", id)
	you_are_my_parent_channels[parent] <- id
	parent_set_channel <- true
	for {
		child, more := <- you_are_my_parent_channels[id]
		if more {
			children = append(children, child)
		} else {
			break
		}
	}
	fmt.Printf("%d)\tparent=%d,\tchildren=%v\n",id, parent, children)
	done_channel <-true
}

func main() {
	edges := readGraph("res/test.txt")
	fmt.Println(edges)
	fmt.Println(len(edges))
	n := len(edges)
	search_channels := make([]chan int, n)
	you_are_my_parent_channels := make([]chan int, n)
	parent_set_channel := make(chan bool, n);
	done_channel := make(chan bool, n)

	for i := 0; i < n; i++ {
		search_channels[i] = make(chan int)
		you_are_my_parent_channels[i] = make(chan int)
	}
	go root(0, you_are_my_parent_channels[0], edges[0], search_channels, parent_set_channel, done_channel)
	for i := 1; i < n; i++ {
		go node(i, edges[i], search_channels, you_are_my_parent_channels, parent_set_channel, done_channel)
	}
	for i := 0; i < n; i++ {
		_ = <-parent_set_channel
	}
	for i := 0; i < n; i++ {
		close(you_are_my_parent_channels[i])
	}
	for i := 0; i < n; i++ {
		_ = <- done_channel
	}
}